﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

-- This script resets the data in the destination database back to the pre-run state

TRUNCATE TABLE dbo.SourceTable
GO

INSERT INTO dbo.SourceTable
(
	Id
	,NameColumn
	,DescriptionColumn
	,DateColumn
)
VALUES
(
	1
	,N'NameValue1'
	,N'DescriptionValue1'
	,CONVERT(datetime, N'2017-04-01 00:00:00:00')
)
,(	2
	,N'NameValue2'
	,N'DescriptionValue2'
	,CONVERT(datetime, N'2017-05-01 00:00:00:00')
)
,(	3
	,N'NameValue3'
	,N'DescriptionValue3'
	,CONVERT(datetime, N'2017-06-01 00:00:00:00')
)
,(	4
	,N'NameValue4'
	,N'DescriptionValue4'
	,CONVERT(datetime, N'2017-07-01 00:00:00:00')
)

GO