﻿CREATE TABLE [dbo].[SourceTable]
(
	[Id] INT NOT NULL PRIMARY KEY
	,NameColumn varchar(10)
	,DescriptionColumn varchar(20)
	,DateColumn datetime
)
