This project is designed to highlight the various problems when developing ETL code with SSIS

#Overview
There are two databases each with one table in each.  The databases were deployed locally with the publish profiles however you will need to create your own profile for your environment.

This solution was created with Visual Studio 2017 and SSDT 2017 Community Edition.  As part of publishing the databases, a post deployment script truncates and loads the test data.  If you want to reset the environment, just publish the destination database again.

The solution contains the Ssis example of moving the data from the source into the destination as well as two stored procedures in the destination showing a the load with a temporary table and one with common table expressions.

Any tool must meet the minimums of easy:
- search
- version
- refactor
- document
- code review
- share
- reuse

#Issues
- Anti-Agile
- Tiny frickin windows
- Clicky, clicky, clicky.  AKA mouse click hell
- Datatype changes
	- [Data type mapping](http://www.sqlservercentral.com/blogs/dknight/2010/12/22/ssis-to-sql-server-data-type-translations/)
- Not MS Database Aware
- All the extra steps for basic process
- To search capbility in the GUI
- Why are the SSIS tools not in the Tool Bar?
- Editing a C#/VB script 
	- Why does editing the script require an entirely new instance of Visual Studio
- Binary sections
- Code Reviews
	- IDE implemented changes
- Error messages
	- Everyone loves for reading C# stacktrace error messages
- Logging
	- Verbose and unreadable
	
#Minor Complaints
- Does not natively talk to MS Access
	- How does an Microsoft product not natively talk to an Office product?
	- The connector provided by Microsoft is 32 bit!!
- Data sources can be created in different context (project or package)
	- There are some edge cases where a package connection would be preferred, but unless you have a coding standard and enforce it religously connection will redundant definitions through your code base.
- Why do regional settings have an effect on the code execution?

#Other Issues
- Security
	- Most people end up storing user/password in the package/project, which means it is stored in plain text (aka XML). :(
- Microsoft Azure does use it 
	- Well until very recently and they have other better solutions for ETL
- Difficult to find a developer
- Technet :(
	- Lots of content with no answers
	- Since Ssis is a GUI text describing something is very difficult to follow
	- If Technet was any good, stackoverflow wouldn't need to exist
- Community Contributors
	- Every problem you encounter has been solved by someone else, but finding somewhere that someone posted the solution is rarer than hen's teeth.

#Demostration
- TBD

#Alternatives
- Stored procedures with SQL Agent jobs
	- Easy to program
	- Easy to debug
	- Easy to maintain
	- Easier to test
	- Searchable inside and outside the IDE
	- Easy to document
	- Easy to source talent
	- Some concurrent execution via SQL Agent jobs calling other SQL Agent jobs.
- Hybrid Ssis calling stored procedures
	- Allows concurrent execution of stored procedures
	- Can pass the system variables to the stored procedures for audit logging if required
	- If you are mandated to use Ssis, you can use the hybrid model to minimize your interaction with it
