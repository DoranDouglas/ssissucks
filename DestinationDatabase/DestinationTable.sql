﻿CREATE TABLE [dbo].[DestinationTable]
(
	[Id] INT NOT NULL PRIMARY KEY
	,NameColumn nvarchar(20)
	,DescriptionColumn nvarchar(50)
	,DateColumn datetime2(7)

	,CdcHash varbinary(64)
)
