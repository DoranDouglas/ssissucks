﻿CREATE PROCEDURE [dbo].[LoadingProcTemp]

/*
*************************************************************************************

Name:   LoadingProcTemp
Description:    Basic example of retrieving data, cleaning it, and populating the destination table with a temp table
Change Detection Strategy: Overwrite
Delete Detection Strategy: None

*************************************************************************************
*/
AS
    DECLARE @InsertCount INT = 0
            ,@UpdateCount INT = 0
            ,@FlagInsert nchar(1) = N'N'
            ,@FlagUpdate nchar(1) = N'U'
            ,@FlagGood nchar(1) = N'G'

    DROP TABLE IF EXISTS #Clean -- This is SQL 2016 functionality
    -- IF OBJECT_ID(N'tempdb..#Clean') IS NOT NULL -- This is prior to SQL 2016 functionality
    -- BEGIN
    --     DROP TABLE #Clean
    -- END

    -- *************************************************************************************
    -- Retrieve and clean the data from the source
    -- *************************************************************************************
    CREATE TABLE #Clean
    (
        Id INT NOT NULL
        ,NameColumn nvarchar(20)
        ,DescriptionColumn nvarchar(50)
        ,DateColumn datetime2(7)
        ,ChangeFlag nchar(1)
    )

    INSERT INTO #Clean
    (
        Id
        ,NameColumn
        ,DescriptionColumn
        ,DateColumn
		,ChangeFlag
    )
    SELECT  src.Id
            ,src.NameColumn
            ,src.DescriptionColumn
            ,src.DateColumn
            ,CASE   WHEN dest.Id IS NULL THEN @FlagInsert
                    WHEN dest.CdcHash IS NULL THEN @FlagUpdate
                    WHEN dest.CdcHash <> src.CdcHash THEN @FlagUpdate
                    ELSE @FlagGood
            END AS ChangeFlag

    FROM    (   SELECT  srcHash.Id AS Id
                        ,srcHash.NameColumn AS NameColumn
                        ,srcHash.DescriptionColumn AS DescriptionColumn
                        ,srcHash.DateColumn AS DateColumn
                        ,HASHBYTES  (   N'SHA2_512'
                                        ,N'{'
                                        + srcHash.NameColumn
                                        + N'|'
                                        + srcHash.DescriptionColumn
                                        + N'|'
                                        + CONVERT(nvarchar(30), srcHash.DateColumn, 121)
                                        + N'}'
                        ) AS CdcHash
                
                FROM    [$(SourceDatabase)].dbo.SourceTable AS srcHash
            ) AS src

        LEFT OUTER JOIN dbo.DestinationTable AS dest
            ON src.Id = dest.Id
    
    -- *************************************************************************************
    -- Apply the updates and insert the new records
    -- *************************************************************************************
    UPDATE dest
        SET NameColumn = src.NameColumn
            ,DescriptionColumn = src.DescriptionColumn
            ,DateColumn = src.DateColumn
            ,CdcHash = src.CdcHash

    FROM    dbo.DestinationTable AS dest

        INNER JOIN #Clean AS src
            ON dest.Id = src.Id
            and src.ChangeFlag = @FlagUpdate

    SET @UpdateCount = @@ROWCOUNT

    INSERT INTO dbo.DestinationTable
    (
        Id
        ,NameColumn
        ,DescriptionColumn
        ,DateColumn
		,CdcHash
    )
    SELECT  src.Id
			,src.NameColumn
			,src.DescriptionColumn
			,src.DateColumn
			,src.ChangeFlag

    FROM    #Clean AS src

    WHERE   1 = 1
            and src.ChangeFlag = @FlagInsert

    SET @InsertCount = @@ROWCOUNT

RETURN 0
