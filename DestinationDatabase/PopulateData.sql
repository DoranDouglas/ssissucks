﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

-- This script resets the data in the destination database back to the pre-run state

TRUNCATE TABLE dbo.DestinationTable
GO

INSERT INTO dbo.DestinationTable
(
	Id
	,NameColumn
	,DescriptionColumn
	,DateColumn
)
VALUES
(
	3
	,N'NameValue3'
	,N'DescriptionValue3'
	,CONVERT(datetime, N'2017-06-01 00:00:00:00')
)	
,(	4
	,N'NameValue4'
	,N'DescriptionValue4'
	,CONVERT(datetime, N'2017-07-01 00:00:00:00')
)

GO

UPDATE dest
	SET CdcHash = HASHBYTES	(	N'SHA2_512'
								,N'{'
                                + dest.NameColumn
                                + N'|'
                                + dest.DescriptionColumn
                                + N'|'
                                + CONVERT(nvarchar(30), dest.DateColumn, 121)
                                + N'}'
							)

FROM	dbo.DestinationTable AS dest

GO