﻿CREATE PROCEDURE [dbo].[LoadingProcCte]

/*
*************************************************************************************

Name:   LoadingProcCte
Description:    Basic example of retrieving data, cleaning it, and populating the destination table with a CTE
Change Detection Strategy: Overwrite
Delete Detection Strategy: None

*************************************************************************************
*/

AS

    -- CTE's don't persist past a single use, so the data set must be split into different datasets for the inserts and updates
    WITH cteSource AS
    (
        SELECT  src.Id
                ,src.NameColumn
                ,src.DescriptionColumn
                ,src.DateColumn

        FROM    [$(SourceDatabase)].dbo.SourceTable AS src
    )
    ,cteInsert AS
    (
        SELECT  src.Id
                ,src.NameColumn
                ,src.DescriptionColumn
                ,src.DateColumn

        FROM    cteSource AS src

            LEFT OUTER JOIN dbo.DestinationTable AS dest
                ON src.Id = dest.Id

        WHERE   1 = 1
                -- Only the records that don't exists yet
                and dest.Id IS NULL
    )
    ,cteUpdate AS
    (
        SELECT  src.Id
                ,src.NameColumn
                ,src.DescriptionColumn
                ,src.DateColumn

        FROM    cteSource AS src
    )

    Update dest
        SET NameColumn = src.NameColumn
            ,DescriptionColumn = src.DescriptionColumn
            ,DateColumn = src.DateColumn

    FROM    dbo.DestinationTable AS dest

        INNER JOIN cteUpdate AS src
            ON dest.Id = src.Id


    INSERT INTO dbo.DestinationTable
    (
        Id
        ,NameColumn
        ,DescriptionColumn
        ,DateColumn
    )
    SELECT  Id
            ,NameColumn
            ,DescriptionColumn
            ,DateColumn

    FROM    cteInsert AS src


RETURN 0
